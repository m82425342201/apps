ln -s /workspace/Apps/dotnet/dotnet /usr/local/bin
ln -s /workspace/Apps/go/bin/* /usr/local/bin
ln -s /workspace/Apps/jdk-18/bin/* /usr/local/bin
ln -s /workspace/Apps/miniconda3/bin/* /usr/local/bin
ln -s /workspace/Apps/ngrok /usr/local/bin
ln -s /workspace/Apps/rebar3 /usr/local/bin
